# Animated Prim Maker #

This is a redesign of the script that originated from the [Outworldz Animated Gif Script](http://www.outworldz.com/cgi/freescripts.plx?ID=535) this script 
will allow any textures generated from their application (which is also stored here in the downloads area) to be animated simply by adding this script, 
and the image into a prim in secondlife.

### Features ###

* Created a simple frame on a prim so you do not have to.
* Simple drag and drop setup. Drag the texture you wish to animate into the prim and it will be displayed on it.
* Compatible with any animated textures in sl. You just need to add the trail onto the end of it. ".anim;Xcuts;Ycuts;FramesPerSecond" without the quotations

### Who do I talk to? ###

* [Kurtis Anatine](https://my.secondlife.com/kurtis.anatine)
