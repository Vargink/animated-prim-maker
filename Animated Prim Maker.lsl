/*
				ANIMATED PRIM MAKER
		Made By Kurits Anatine
	Latest Version at https://bitbucket.org/Vargink/lsl-animated-prim-maker Make sure to fork!
  
  This script works with the gif 2 sl application which is available at link above or simply put .anim;Xcuts;Ycuts;FramesPerSecond onto the end of the picture.
*/


default
{
    state_entry()
    {
        llSetTexture(TEXTURE_BLANK,ALL_SIDES);
        llSetColor(<0.0, 0.0, 0.0>,ALL_SIDES);
        llSetColor(<1.0, 1.0, 1.0>,2);
        llSetScale(<0.05, 2.00, 1.5>);
		llSetObjectName("Animated Prim");
        llSetPrimitiveParams( [ PRIM_FULLBRIGHT,2,TRUE]);
        llSay(0, "Hello!, Please add your texture you have made with https://bitbucket.org/Vargink/lsl-animated-prim-maker into me and i will make it animated!");
    }
    changed(integer num)
    {
        if (num & CHANGED_INVENTORY)
        {
            if(llGetInventoryNumber(INVENTORY_TEXTURE) == 0)
            {
                llSay(0,"Nothing in here :( try adding a texture (full perms only please!)");
                llSetTexture(TEXTURE_BLANK,2);
                llSetTextureAnim(FALSE, 2, 0, 0, 0.0, 0.0, 1.0);
            }
            else
            {
                if (llGetInventoryKey(llGetInventoryName(INVENTORY_TEXTURE,0)) == NULL_KEY)
                {
                    llSay(0, "I'm Sorry but the texture has to be full permissions to be able to be used :(");
                }
                else
                {
                    list tempinfo = llParseString2List(llGetInventoryName(INVENTORY_TEXTURE, 0),[";"],[]);
                    llSetTexture(llGetInventoryKey(llGetInventoryName(INVENTORY_TEXTURE, 0)),2);
                    llSetTextureAnim(ANIM_ON | LOOP, 2, llList2Integer(tempinfo,1), llList2Integer(tempinfo,2), 0, 0,llList2Float(tempinfo,3));
                }
            }
        }
    }
}